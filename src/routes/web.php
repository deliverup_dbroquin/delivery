<?php

Route::prefix('delivery')->group(function() {
    Route::get('tiles', 'Deliverup\Delivery\TileController@index');
    Route::get('datatable', 'Deliverup\Delivery\TourController@index');

    Route::prefix('search')->group(function() {
        Route::get('employers', 'Deliverup\Delivery\EmployerController@index');
        Route::get('statuses/{type}', 'Deliverup\Delivery\StatusController@index');
        Route::get('tours', 'Deliverup\Delivery\TourController@index');
    });
});