<?php

namespace Deliverup\Delivery;

use Illuminate\Routing\Controller as BaseController;

class CustomerController extends BaseController
{
    /**
     * Grab all customers
     *
     * @return
     */
    public function index()
    {

    }

    /**
     * Show specific customer details
     *
     * @return
     */
    public function show()
    {

    }
}