<?php

namespace Deliverup\Delivery;

use Illuminate\Routing\Controller as BaseController;

class StepController extends BaseController
{
    /**
     * Grab all tours
     *
     * @return
     */
    public function index()
    {

    }

    /**
     * Show specific step details
     *
     * @return
     */
    public function show()
    {

    }
}