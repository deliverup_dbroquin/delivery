<?php

namespace Deliverup\Delivery;

use Illuminate\Routing\Controller as BaseController;
use Deliverup\Pegasus\User;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use dukmaurice\fuel\Entities\MeasureType;
use Deliverup\Delivery\Traits\DeliveryTourRelated;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use dukmaurice\fuel\Entities\Tour;
use dukmaurice\fuel\Entities\Shift;

class TourController extends BaseController
{
    use DeliveryTourRelated;

    /**
     * Grab all tours
     *
     * @return
     */
    public function index(Request $request)
    {
        $tours = DB::table('tours as TRN')
            ->leftJoin('measures AS DTE', function ($join) {
                $join
                ->on('DTE.model_id', 'TRN.id')
                ->where('DTE.model_type', Tour::class)
                ->where('DTE.type_id', $this->measureTypes()->planned_date);
            })
            ->leftJoin('measures AS DTE_START', function ($join) {
                $join
                ->on('DTE_START.model_id', 'TRN.id')
                ->where('DTE_START.model_type', Tour::class)
                ->where('DTE_START.type_id', $this->measureTypes()->datetime_start);
            })
            ->leftJoin('measures AS DTE_END', function ($join) {
                $join
                ->on('DTE_END.model_id', 'TRN.id')
                ->where('DTE_END.model_type', Tour::class)
                ->where('DTE_END.type_id', $this->measureTypes()->datetime_end);
            })
            ->leftJoin('terminals AS TERM', 'TERM.id', '=', 'TRN.terminal_id')
            ->leftJoin('states AS STATE', 'STATE.id', '=', 'TRN.state_id')
            ->leftJoin('shifts AS SHIFT', 'SHIFT.id', '=', 'TRN.shift_id')
            ->leftJoin('measures AS DTE_SHIFT_START', function ($join) {
                $join
                ->on('DTE_SHIFT_START.model_id', 'SHIFT.id')
                ->where('DTE_SHIFT_START.model_type', Shift::class)
                ->where('DTE_SHIFT_START.type_id', $this->measureTypes()->datetime_start);
            })
            ->leftJoin('measures AS DTE_SHIFT_END', function ($join) {
                $join
                ->on('DTE_SHIFT_END.model_id', 'SHIFT.id')
                ->where('DTE_SHIFT_END.model_type', Shift::class)
                ->where('DTE_SHIFT_END.type_id', $this->measureTypes()->datetime_end);
            })
            ->leftJoin('resources AS CONTRACT_RES', function ($join) {
                $join
                ->on('CONTRACT_RES.parent_id', 'SHIFT.id')
                ->where('CONTRACT_RES.parent_type', Shift::class)
                ->where('CONTRACT_RES.type_id', $this->resourceTypes()->contract);
            })
            ->leftJoin('contracts AS CONTRACT', 'CONTRACT.id', '=', 'CONTRACT_RES.model_id')
            ->leftJoin('employers AS EMP', 'EMP.id', '=', 'CONTRACT.employer_id')
            ->leftJoin('resources AS TRUCK_RES', function ($join) {
                $join
                ->on('TRUCK_RES.parent_id', 'SHIFT.id')
                ->where('TRUCK_RES.parent_type', Shift::class)
                ->where('TRUCK_RES.type_id', $this->resourceTypes()->truck);
            })
            ->leftJoin('codes AS TRN_VERSION', function ($join) {
                $join
                ->on('TRN_VERSION.model_id', 'TRN.id')
                ->where('TRN_VERSION.model_type', Tour::class)
                ->where('TRN_VERSION.code_id', $this->codeTypes()->trip_number);
            })
            ->leftJoin('trucks AS TRUCK', 'TRUCK.id', '=', 'TRUCK_RES.model_id')
            ->leftJoin('devices AS DEV', 'DEV.id', '=', 'TRN.request_device_id')
            ->selectRaw('TRN.id AS tour_id, TRN.is_locked AS is_locked, TRN_VERSION.value AS tour_version, TRN.version AS version, DTE.value as realdate,DTE_START.value AS datetime_start, DTE_END.value AS datetime_end, TRN.code AS tour_code, TERM.name AS terminal_name, STATE.code AS state_code, STATE.color AS state_color, SHIFT.code AS shift_code,DTE_SHIFT_START.value AS datetime_shift_start, DTE_SHIFT_END.value AS datetime_shift_end, EMP.name AS employer_name, TRUCK.registration AS registration, DEV.imei AS imei, DEV.username AS username, DEV.version_app AS version_app');

        $tours->whereBetween('DTE.value', [
            $request->period[0],
            $request->period[1]
        ]);

        $tours->groupBy('TRN.id');

        return Datatables::of($tours)
            ->addColumn('steps', function ($tour) {
                return $this->getStepTour($tour->tour_id);
            })
            ->toJson();
    }

    /**
     * Show specific tour details
     *
     * @return
     */
    public function show()
    {

    }
}