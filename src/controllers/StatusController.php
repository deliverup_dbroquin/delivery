<?php

namespace Deliverup\Delivery;

use Illuminate\Routing\Controller as BaseController;
use dukmaurice\fuel\Entities\State;

class StatusController extends BaseController
{
    /**
     * Grab all status
     *
     * @return
     */
    public function index($type)
    {
        return State::where('code', 'LIKE', $type.'%')->get();
    }
}