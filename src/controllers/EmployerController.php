<?php

namespace Deliverup\Delivery;

use Illuminate\Routing\Controller as BaseController;
use dukmaurice\fuel\Entities\Employer;

class EmployerController extends BaseController
{
    /**
     * Grab all employers
     *
     * @return
     */
    public function index()
    {
        return Employer::all();
    }

    /**
     * Show specific employer details
     *
     * @return
     */
    public function show()
    {

    }
}