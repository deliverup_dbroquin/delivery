<?php

namespace Deliverup\Delivery\Traits;

use Illuminate\Support\Facades\Cache;
use dukmaurice\fuel\Entities\MeasureType;
use dukmaurice\fuel\Entities\ResourceType;
use dukmaurice\fuel\Entities\CodeType;

trait DeliveryTourRelated
{
    /**
     * Get cached (put in cache for one hour if not exist) measure types
     *
     * @return dukmaurice\fuel\Entities\MeasureType
     */
    protected function measureTypes()
    {
        return Cache::remember('tours.measure_types', 60, function() {
            return $this->relatedFetcher(MeasureType::query(), [
                'real_date',
                'planned_date',
                'datetime_start',
                'datetime_end',
                'delivery_manual',
                'delivery_remote'
            ]);
        });
    }

    /**
     * Get cached (put in cache for one hour if not exist) resource types
     *
     * @return dukmaurice\fuel\Entities\ResourceType
     */
    protected function resourceTypes()
    {
        return Cache::remember('tours.resource_types', 60, function() {
            return $this->relatedFetcher(ResourceType::query(), ['contract', 'truck']);
        });
    }

    /**
     * Get cached (put in cache for one hour if not exist) code types
     *
     * @return dukmaurice\fuel\Entities\CodeType
     */
    protected function codeTypes()
    {
        return Cache::remember('tours.code_types', 60, function() {
            return $this->relatedFetcher(CodeType::query(), ['trip_number']);
        });
    }

    /**
     * Prepare model to be cached
     *
     * @param Illuminate\Database\Eloquent\Model $model
     * @param array $toSearch
     * @return void
     */
    protected function relatedFetcher($model, $toSearch)
    {
        $toStore = new \stdClass();
        $items = $model->whereIn('code', $toSearch)->get();

        foreach($items as $item) {
            $toStore->{$item->code} = $item->id;
        }

        return $toStore;
    }
}