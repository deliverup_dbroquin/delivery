<?php

namespace Deliverup\Delivery\Traits;

use Illuminate\Support\Facades\Cache;


trait DeliveryStep
{
    /**
     * Get step for specific tour
     * Put in cache to avoid massive query on the fly
     *
     * @param integer $id
     * @return void
     */
    protected function stepFetcher($id)
    {
        return Cache::remember('tours-' . $id . '-steps', 3, function() use($id) {
            return $this->stepFormer($id);
        });
    }

    /**
     * String query for planned dates
     *
     * @return void
     */
    protected function getPlanned()
    {
        return '(SELECT SUM(measures.value) FROM tasks INNER JOIN measures ON measures.model_id = tasks.id AND measures.model_type = "dukmaurice\\\fuel\\\Entities\\\Task" WHERE parent_id = STEP.id AND parent_type = "dukmaurice\\\fuel\\\Entities\\\Step" AND measures.type_id = 8 AND tasks.type_id = 1) AS qty_planned';
    }

    /**
     * String query for real dates
     *
     * @return string
     */
    protected function getReal()
    {
        return '(SELECT SUM(measures.value) FROM tasks INNER JOIN measures ON measures.model_id = tasks.id AND measures.model_type = "dukmaurice\\\fuel\\\Entities\\\Task" WHERE parent_id = STEP.id AND parent_type = "dukmaurice\\\fuel\\\Entities\\\Step" AND measures.type_id = 8 AND tasks.type_id = 2) AS qty_real';
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function getSteps()
    {

    }

    /**
     * Return well formated steps
     *
     * @param integer $id
     * @return void
     */
    protected function stepFormer($id)
    {
        $toReturn = [];

        foreach($steps as $step) {
            $array_step = [];

            foreach ($step as $id => $value) {
                $array_step[$id] = $value;
            }

            $array_step['state_name'] = trans('fuel_connect.libelle_state.' . $step->state_code);

            $steps[] = $array_step;
        }

        return $toReturn;
    }
}