const state = {
    period: [],
    employers: [],
    statuses: [],
    refresh: false,
    initial: false
}

const mutations = {
    setSearch(state, { type, items }) {
        state[type] = items
    },
    setToRefresh(state) {
        state.refresh = true
    },
    setToRefreshed(state) {
        state.refresh = false
    },
    setToShow(state) {
        state.initial = true
    }
}

export default {
    namespaced: true,
    state,
    mutations
}