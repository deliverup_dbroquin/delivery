export default {
    table: {
        columns: [
            {
                title: 'delivery.table.columns.date',
                data: 'realdate',
                orderable: true,
                date: true
            },
            {
                title: 'delivery.table.columns.code',
                data: 'tour_code',
                orderable: true
            },
            {
                title: 'delivery.table.columns.terminal',
                data: 'terminal_name',
                orderable: true
            },
            {
                title: 'delivery.table.columns.employer',
                data: 'employer_name',
                orderable: true
            },
            {
                title: 'delivery.table.columns.immat',
                data: 'registration',
                orderable: true
            },
            {
                title: 'delivery.table.columns.orders',
                data: 'orders',
                orderable: false
            },
            {
                title: 'delivery.table.columns.progress',
                data: 'progress',
                orderable: false
            },
            {
                title: 'delivery.table.columns.status',
                data: 'state_code',
                orderable: true
            }
        ]
    }
}