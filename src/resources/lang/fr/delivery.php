<?php

return [
    'title' => 'Suivi des livraisons',
    'search' => [
        'period' => 'Période de recherche',
        'employer' => 'Filiale(s)',
        'status' => 'Statut(s)',
        'tour' => 'Numéro de tournée'
    ],
    'tiles' => [
        'default' => 'Tournées non démarrées',
        'progress' => 'Tournées en cours',
        'end' => 'Tournées terminées'
    ],
    'table' => [
        'header' => [
            'title' => 'Tournées',
            'subtitle' => 'Avancement des tournées en fonction de la recherche'
        ],
        'columns' => [
            'date' => 'Date',
            'code' => 'Code',
            'terminal' => 'Dépôt',
            'employer' => 'Transporteur',
            'immat' => 'Immat.',
            'orders' => 'Commande(s)',
            'progress' => 'Avancée',
            'status' => 'Statut'
        ]
    ]
];