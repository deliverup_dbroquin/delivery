<?php

return [
    'title' => 'Delivery Follow Up',
    'search' => [
        'period' => 'Search range',
        'employer' => 'Subsidiary company(ies)',
        'status' => 'Status(es)',
        'tour' => 'Tour number'
    ],
    'tiles' => [
        'default' => 'Non-started tours',
        'progress' => 'Tours in progress',
        'end' => 'Tours ended'
    ],
    'table' => [
        'header' => [
            'title' => 'Tours',
            'subtitle' => 'Research based tours progression'
        ],
        'columns' => [
            'date' => 'Date',
            'code' => 'Code',
            'terminal' => 'Terminal',
            'employer' => 'Employer',
            'immat' => 'Immat.',
            'orders' => 'Order(s)',
            'progress' => 'Progress',
            'status' => 'Status'
        ]
    ]
];