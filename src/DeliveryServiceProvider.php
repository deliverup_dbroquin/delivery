<?php

namespace Deliverup\Delivery;

use Illuminate\Support\ServiceProvider;


class DeliveryServiceProvider extends ServiceProvider
{
     /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $jsPath = 'assets/js/';

        /** -----------------------------------------
         * Loads
         */
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');

        /** -----------------------------------------
         * Publishes
         */

        // Translations
        $this->publishes([
            __DIR__ . '/resources/lang/en' => resource_path('lang/en'),
            __DIR__ . '/resources/lang/fr' => resource_path('lang/fr')
        ], 'delivery-translations');

        // View
        $this->publishes([
            __DIR__ . '/resources/view' => resource_path($jsPath . 'views/modules/default')
        ], 'delivery-view');

        // Configuration
        $this->publishes([
            __DIR__ . '/resources/config' => resource_path($jsPath . 'config')
        ], 'delivery-config');

        // Components
        $this->publishes([
            __DIR__ . '/resources/components' => resource_path($jsPath . 'components/delivery')
        ], 'delivery-components');

        // Store
        $this->publishes([
            __DIR__ . '/resources/store' => resource_path($jsPath . 'stores/modules')
        ], 'delivery-store');

        // Controllers
        $this->publishes([
            __DIR__ . '/resources/store' => resource_path($jsPath . 'stores/modules')
        ], 'delivery-controllers');

        // Seeders
        $this->publishes([
            __DIR__ . '/database/seeds' => database_path('seeds')
        ], 'delivery-seeds');

        // Routes
        $this->publishes([
            __DIR__ . '/database/seeds' => database_path('seeds')
        ], 'delivery-seeds');
    }

     /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}