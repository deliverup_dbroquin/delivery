<?php

use Illuminate\Database\Seeder;
use Deliverup\Pegasus\Module;
use Deliverup\Pegasus\Group;

class CreateDeliveryAccess extends Seeder
{
    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        $deliveryModule = Module::where('code', 'delivery')->first();

        foreach (Group::all() as $group) {
            $group->modules()->syncWithoutDetaching($deliveryModule->id);
        };
    }
}