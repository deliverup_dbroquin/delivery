<?php

use Illuminate\Database\Seeder;
use Deliverup\Pegasus\Module;
use Deliverup\Pegasus\Section;

class CreateDeliveryModule extends Seeder
{
    /**
     * Run the database seeds
     *
     * @return void
     */
    public function run()
    {
        Module::firstOrCreate(
            [
                'code' => 'delivery',
                'name' => 'delivery.title',
                'icon' => 'truck',
                'order' => 110,
                'link' => 'delivery',
                'section_id' => Section::firstOrCreate(['code' => 'default', 'order' => 10])->id
            ]
        );
    }
}